---
title: Surcharge bettik
date: 2020-04-28 10:00:00
resolved: false
# resolvedWhen: 2020-10-05 16:58:00
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - Bettik
  - Luke 
  - Dahu
section: issue
---

There may be disruptions in the rollout.