---
title: Maintenance et mise à jour des OS
date: 2019-12-01 15:54:00
resolved: true
resolvedWhen: 2020-01-01 16:54:00
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - Luke
  - Dahu
  - Cigri
section: issue
---

*Just began* - We're currently shutting down the network. {{< track "2018-06-13 15:54:00" >}}
